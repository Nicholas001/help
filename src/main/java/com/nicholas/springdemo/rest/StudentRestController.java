package com.nicholas.springdemo.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nicholas.springdemo.entity.Student;

@RestController
@RequestMapping("/api")
public class StudentRestController {

	@GetMapping("/students")
	public List<Student> getStudents() {

		List<Student> students = new ArrayList<>();

		students.add(new Student("Calina", "Ana"));
		students.add(new Student("Amelia", "Andreea"));
		students.add(new Student("Nicu", "Marian"));

		return students;
	}
}
